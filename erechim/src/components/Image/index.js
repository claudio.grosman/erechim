import React,{Fragment} from 'react';
import {Image} from './styles'

const Images = ({hidden}) => (

    <Fragment>
    <Image hidden={false} cubo={false}>
         <img src="https://aberturasimples.com.br/wp-content/uploads/2018/08/abrir-empresa-em-erechim.jpg" alt="Erechim" className="Img"/>
     </Image>   
     <Image hidden={hidden} cubo={true}>
     <img src="http://2.bp.blogspot.com/-Xegq866caf0/U1RsRqESP-I/AAAAAAAAAfQ/bGOcdKootY0/s1600/Cubo+m%C3%A1gico.png" alt=""/>
  </Image>   
  </Fragment>
  
)

export default Images;
