import styled from 'styled-components';

export const Image = styled.div`
    top: ${({cubo})=>cubo?`${Math.random() * 850}px`:''};
    left: ${({cubo})=>cubo?`${Math.random() * 1800}px`:''};
    position: ${({cubo}) => cubo?'absolute':''};
 &>img {
    width: ${({cubo}) => cubo? '100px':'1600px'};
    height: ${({cubo}) => cubo? '100px':'700px'};
    display: ${({hidden}) => hidden?'none':'block'};
 }
`;
