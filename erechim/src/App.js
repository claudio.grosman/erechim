import React, { Component } from 'react';
import './App.css';
import Images from './components/Image'

class App extends Component {
  constructor(props){
    super(props);

    this.state = { message: "",cubo:false}
  }

  verificar = ()=>{
    let message = this.state.message
    if (message === "Cubo mágico"){
      this.setState({cubo: true})
      console.log("Executando função cubo mágico");
    
    }if (message === "Morrer"){
      this.setState({cubo:false})
      console.log("Executando função morrer");

    }
  }

    render() {
    return (

      <div className="App">
        <p className="Text"> Aplicação Aleatória </p>
          <textarea onChange={(evt)=> {
            let message = evt.target.value;
            this.setState({message});
          }}
          className="textarea" />
          <br/>

          <div>
            <button onClick={this.verificar}> OK </button>
          </div>
          <br/>
         
           <Images hidden={!this.state.cubo}/>
            
        
      </div>
    );
  }
}

export default App;
